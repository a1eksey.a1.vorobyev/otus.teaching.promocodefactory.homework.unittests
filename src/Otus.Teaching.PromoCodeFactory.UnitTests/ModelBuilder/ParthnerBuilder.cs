﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class ParthnerBuilder
    {
        public Partner partner { get; set; }
        public ParthnerBuilder()
        {
            partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = DateTime.Today.AddDays(-4),
                        //CancelDate =  DateTime.Today.AddDays(-2),
                        EndDate = DateTime.Today,
                        Limit = 100
                    }
                }
            };
        }
        public ParthnerBuilder(DateTime createDate,DateTime cancelDate, DateTime endDate)
        {
            partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = createDate,
                        CancelDate = cancelDate,
                        EndDate = endDate,
                        Limit = 100
                    }
                }
            };
        }
        public void setParametrsWhithActiveLimit()
        {
            partner.NumberIssuedPromoCodes = 20;
        }
    }
}
