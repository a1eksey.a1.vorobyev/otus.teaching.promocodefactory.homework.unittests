﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.ModelBuilder
{
    public static class Extention
    {
        public static Partner ResetNumberIssuedPromoCodes(this Partner partner)
        {
            partner.NumberIssuedPromoCodes = 0;

            return partner;
        }

        public static Partner SetNumberIssuedPromoCodes(this Partner partner, int promoCode)
        {
            partner.NumberIssuedPromoCodes = promoCode;

            return partner;
        }

        public static PartnersController CreatePartnersController(this Partner partner, Guid partnerId)
        {
            Mock<IRepository<Partner>> mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(DateTime.Today);
            PartnersController controller = new PartnersController(mock.Object, mockCurrentDateTimeProvider.Object);
            return controller;
        }


    }
}
