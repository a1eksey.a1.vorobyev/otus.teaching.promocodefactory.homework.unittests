﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.ModelBuilder;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        //TODO: Add Unit Tests
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            Partner partner = null;

            var controller = partner.CreatePartnersController(partnerId);

            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = new ParthnerBuilder().partner;
            partner.IsActive = false;

            var controller = partner.CreatePartnersController(partnerId);
            // Act
            var result = await controller.CancelPartnerPromoCodeLimitAsync(partnerId);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ZeroingNumberIssuedPromoCodes()
        {
            //Arrange
            var partner = new ParthnerBuilder().partner.SetNumberIssuedPromoCodes(20);
            var partnerId = partner.Id;
            
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            var expected = new ParthnerBuilder().partner.ResetNumberIssuedPromoCodes();

            var controller = partner.CreatePartnersController(partnerId);

            //Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(expected.NumberIssuedPromoCodes);
        }


        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ValidModel_SuccessUpdate()
        {
            //Arrange
            var partner = new ParthnerBuilder().partner;
            var partnerId = partner.Id;
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(DateTime.Now);

            var controller = new PartnersController(mock.Object, mockCurrentDateTimeProvider.Object);

            //Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            mock.Verify(x => x.UpdateAsync(partner), Times.Once);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NegativeLimit_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            request.Limit = -10;

            //var partner = new ParthnerBuilder(DateTime.Today.AddDays(-4), DateTime.Today.AddDays(-2), DateTime.Today).partner;
            var partner = new ParthnerBuilder().partner;

            //_partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            var controller = partner.CreatePartnersController(partnerId);

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
            // Act
            //var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerSetLimit_NotZeroingNumberIssuedPromoCodes()
        {
            //Arrange
            var partner = new ParthnerBuilder(DateTime.Today.AddDays(-4), DateTime.Today.AddDays(-2), DateTime.Today).partner.SetNumberIssuedPromoCodes(20); 
            var partnerId = partner.Id;
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            var expected = new ParthnerBuilder().partner.SetNumberIssuedPromoCodes(20);

            var controller = partner.CreatePartnersController(partnerId);

            //Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(expected.NumberIssuedPromoCodes);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldSetCancelDateNow()
        {
            //Arrange
            var partner = new ParthnerBuilder().partner; 
            var targetLimit = partner.PartnerLimits.First();
            var partnerId = partner.Id;
            var request = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            var expected = new ParthnerBuilder().partner.PartnerLimits.First();
            expected.CancelDate = DateTime.Today;

            //Act

            //var controller = new PartnersController(mock.Object, mockCurrentDateTimeProvider.Object);
            var controller = partner.CreatePartnersController(partnerId);

            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            targetLimit.Should().BeEquivalentTo(expected);
        }

    }
}